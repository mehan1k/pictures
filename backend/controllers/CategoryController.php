<?php
namespace backend\controllers;

use common\models\Category;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


/**
 * Site controller
 */
class CategoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'category', 'edit_category'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionCategory()
    {
        $category = new Category();


        if ($category->load(Yii::$app->request->post()) && $category->save()) {

            $all_category = Category::find()->all();

            return $this->render('category', [
                'all_category' => $all_category,
                'category' => $category,
            ]);
        }

        $all_category = Category::find()->all();

        return $this->render('category', [

            'all_category' => $all_category,
            'category' => $category,


        ]);


    }


    public function actionEdit_category($id)
    {
        $edit_category = Category::findOne($id);

        if ($edit_category->load(Yii::$app->request->post()) && $edit_category->save()) {

            Yii::$app->session->setFlash('success', 'Запис змінено');

            return $this->redirect(['edit_category', 'id' => $edit_category->id]);
        } else {
            return $this->render('edit_category', [
                'edit_category' => $edit_category,
            ]);
        }
    }

}



