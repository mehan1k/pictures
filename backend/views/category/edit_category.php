<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


$this->title = 'Редактор категоріями';

?>

<?php if (Yii::$app->session->hasFlash('success')): ?>
    <?= Yii::$app->session->getFlash('success') ?>
<?php endif; ?>



<div class="post-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($edit_category, 'title')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton($edit_category->isNewRecord ? 'Create' : 'Update', ['class' => $edit_category->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
