<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


$this->title = 'Редактор категоріями';
?>


<div class="site-index">


    <ul>
        <?php foreach ($all_category as $one_category): ?>
            <br>
        <div class="badge badge2">
            <li>
                <?= Html::encode("{$one_category->title} ") ?>
            </li>
            </div>

            <a href="<?= \yii\helpers\Url::to(['category/edit_category', 'id'=>$one_category->id]) ?>">Редагувати</a>

        <?php endforeach; ?>
    </ul>



    <?php

    $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],

    ]) ?>

    <?= $form->field($category, 'title') ?>


    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Добавити Категорію', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end() ?>



</div>