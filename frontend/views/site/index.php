<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Редактор картинок';
?>
<div class="site-index">
    <br>

    <?php foreach ($category as $one_category): ?>
        <div class="badge badge2">
            <a href="<?= Url::to(['site/index', 'id'=>$one_category->id ]) ?>"> <?= $one_category->title ?>     </a>
        </div>
    <?php endforeach; ?>


    <div class="row">
        <?php foreach ($pictures as $one_pictures): ?>

            <div class="col-md-5 col-xs-5" style="float: left; margin: 15px; ">

                <a href="/pictures/<?= $one_pictures->img ?>"><?= Html::img('@web/pictures/img/'.$one_pictures->img, ['class'=>'col-md-5 col-xs-5']) ?></a>
                Назва:
                <?= Html::encode("{$one_pictures->title} ") ?>
                <br>
                Розмір:
                <?= Html::encode("{$one_pictures->size} ") ?>
                байт
                <br>
                Дата добавлення:
                <?= Yii::$app->formatter->asDate($one_pictures->created_at) ?>
                <br>
                Категорія:
                <?= $one_pictures->category->title ?>
                <br>

                <a href="<?= Url::to(['site/index', 'delete'=>$one_pictures->img]) ?>" onclick="return confirm('Ви дійсно хочете вилучити картинрку?') ? true : false;"  > Видалити </a>

            </div>

        <?php endforeach; ?>
    </div>
    <br>
    <div class="badge badge2">
        <a href="<?= Url::to(['site/create']) ?>">Добавити картинку</a>
    </div>



</div>
