<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;


$this->title = 'добавити картинку';
?>


<div class="col-md-5">

    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <?= Yii::$app->session->getFlash('success') ?>
    <?php endif; ?>

    <?php

    $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data'],

    ]) ;

    ?>

    <?= $form->field($pictures, 'title')->hint('Будь ласка, ведіть назву картинки')->label('Назва картинки') ?>

    <?= $form->field($pictures, 'category_id')->dropDownList($category)->label('Категорія') ?>

    <?= $form->field($pictures, 'file')->fileInput()->label('Картинка') ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Добавити Картинку', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end() ?>


</div>




