<?php

use yii\db\Migration;

/**
 * Handles adding size to table `pictures`.
 */
class m171108_092358_add_size_column_to_pictures_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('pictures', 'size', $this->integer(11)->notNull());

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('pictures', 'size');
    }
}
