<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pictures`.
 */
class m171103_103532_create_pictures_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('pictures', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'img' => $this->string(255)->notNull(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
            'category_id' => $this->integer(11)->notNull(),


        ],'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->createIndex(
            'idx-pictures-category_id',
            'pictures',
            'category_id'
        );

        $this->addForeignKey(
            'fk-pictures-category_id',
            'pictures',
            'category_id',
            'category',
            'id',
            'CASCADE',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-pictures-category_id',
            'pictures'
        );

        $this->dropIndex(
            'idx-pictures-category_id',
            'pictures'
        );
        $this->dropTable('pictures');
    }
}
